Text by John Anthony and images by Stacey Sanderson are released under a
Creative Commons Attribution-NonCommercial 3.0 Unported license (CC BY-NC 3.0)

Old-School Essentials is a trademark of Necrotic Gnome. The trademark and
Old-School Essentials logo are used with permission of Necrotic Gnome, under
license.
